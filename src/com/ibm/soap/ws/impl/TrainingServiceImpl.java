package com.ibm.soap.ws.impl;

import javax.jws.WebService;

import com.ibm.soap.model.Training;
import com.ibm.soap.ws.TrainingService;

public class TrainingServiceImpl implements TrainingService {

	@Override
	public Training getData(String name) {
		Training training = new Training();
		training.setAge(21);
		training.setId(1);
		training.setName(name);
		return training;
	}

	@Override
	public Training getAll(int age) {
		Training training = new Training();
		training.setAge(age);
		training.setId(2);
		training.setName("cams");
		return training;
	}

}
