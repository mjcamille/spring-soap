package com.ibm.soap.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.ibm.soap.model.Training;

@WebService(serviceName="TrainingService")
public interface TrainingService {

	@WebMethod(operationName="getData")
	@WebResult(name="Training")
	public Training getData(@WebParam(name="name") String name);
	
	@WebMethod(operationName="getAll")
	@WebResult(name="Training")
	public Training getAll(@WebParam(name="age") int age);
}
